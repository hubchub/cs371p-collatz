// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}
TEST(CollatzFixture, read_2) {
    string s("1000000 1\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1000000);
    ASSERT_EQ(p.second, 1);
}
TEST(CollatzFixture, read_3) {
    string s("69 420\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   69);
    ASSERT_EQ(p.second, 420);
}
TEST(CollatzFixture, read_4) {
    string s("666 999\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   666);
    ASSERT_EQ(p.second, 999);
}
TEST(CollatzFixture, read_5) {
    string s("1 19980\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 19980);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

// -----
// print
// -----

TEST(CollatzFixture, print_1) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

TEST(CollatzFixture, print_2) {
    ostringstream w;
    collatz_print(w, 374706, 763315, 509);
    ASSERT_EQ(w.str(), "374706 763315 509\n");
}
TEST(CollatzFixture, print_3) {
    ostringstream w;
    collatz_print(w, 654099, 894492, 525);
    ASSERT_EQ(w.str(), "654099 894492 525\n");
}
TEST(CollatzFixture, print_4) {
    ostringstream w;
    collatz_print(w, 266683, 785376, 509);
    ASSERT_EQ(w.str(), "266683 785376 509\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve_1) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
TEST(CollatzFixture, solve_2) {
    istringstream r("590606 459246\n102778 727957\n336812 674371\n848572 828534\n952330 818322\n214391 827230\n674364 919261\n289358 631624\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("590606 459246 470\n102778 727957 509\n336812 674371 509\n848572 828534 525\n952330 818322 525\n214391 827230 509\n674364 919261 525\n289358 631624 509\n", w.str());
}
TEST(CollatzFixture, solve_3) {
    istringstream r("488669 549965\n358805 288485\n82186 798411\n539800 354124\n704498 695817\n741399 576128\n606400 307416\n314781 729440\n576547 832543\n428923 320288\n326183 721821\n526411 282069\n615390 887948\n304794 774990\n875014 51703\n462898 914520\n918606 395471\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("488669 549965 470\n358805 288485 441\n82186 798411 509\n539800 354124 470\n704498 695817 411\n741399 576128 509\n606400 307416 470\n314781 729440 509\n576547 832543 509\n428923 320288 449\n326183 721821 509\n526411 282069 470\n615390 887948 525\n304794 774990 509\n875014 51703 525\n462898 914520 525\n918606 395471 525\n", w.str());
}
